INCLUDEPATH += \
    $$PWD \
    $$PWD/Button \
    $$PWD/ListView \
    $$PWD/Other \
    $$PWD/RightClickMenu \
    $$PWD/ViewItem \
    $$PWD/Widget

HEADERS += \
    $$PWD/Button/function_classify_button.h \
    $$PWD/Button/letter_classify_button.h \
    $$PWD/Button/tool_button.h \
    $$PWD/ListView/fulllistview.h \
    $$PWD/ListView/klistview.h \
    $$PWD/ListView/listview.h \
    $$PWD/ListView/rightlistview.h \
    $$PWD/ListView/tabletlistview.h \
    $$PWD/Other/classify_btn_scrollarea.h \
    $$PWD/Other/lettertooltip.h \
    $$PWD/Other/scrollarea.h \
    $$PWD/RightClickMenu/menubox.h \
    $$PWD/RightClickMenu/rightclickmenu.h \
    $$PWD/RightClickMenu/tabletrightclickmenu.h \
    $$PWD/ViewItem/full_item_delegate.h \
    $$PWD/ViewItem/itemdelegate.h \
    $$PWD/ViewItem/kitemdelegate.h \
    $$PWD/ViewItem/recent_item_delegate.h \
    $$PWD/ViewItem/right_item_delegate.h \
    $$PWD/ViewItem/tablet_full_itemdelegate.h \
    $$PWD/Widget/animationpage.h \
    $$PWD/Widget/full_commonuse_widget.h \
    $$PWD/Widget/full_function_widget.h \
    $$PWD/Widget/full_letter_widget.h \
    $$PWD/Widget/full_searchresult_widget.h \
    $$PWD/Widget/function_Widget.h \
    $$PWD/Widget/function_button_widget.h \
    $$PWD/Widget/letter_button_widget.h \
    $$PWD/Widget/main_view_widget.h \
    $$PWD/Widget/plugin_widget.h \
    $$PWD/Widget/splitbar_frame.h \
    $$PWD/Widget/tabview_widget.h \
    $$PWD/full_mainwindow.h \
    $$PWD/mainwindow.h \
    $$PWD/tabletwindow.h

SOURCES += \
    $$PWD/Button/function_classify_button.cpp \
    $$PWD/Button/letter_classify_button.cpp \
    $$PWD/Button/tool_button.cpp \
    $$PWD/ListView/fulllistview.cpp \
    $$PWD/ListView/klistview.cpp \
    $$PWD/ListView/listview.cpp \
    $$PWD/ListView/rightlistview.cpp \
    $$PWD/ListView/tabletlistview.cpp \
    $$PWD/Other/classify_btn_scrollarea.cpp \
    $$PWD/Other/lettertooltip.cpp \
    $$PWD/Other/scrollarea.cpp \
    $$PWD/RightClickMenu/menubox.cpp \
    $$PWD/RightClickMenu/rightclickmenu.cpp \
    $$PWD/RightClickMenu/tabletrightclickmenu.cpp \
    $$PWD/ViewItem/full_item_delegate.cpp \
    $$PWD/ViewItem/itemdelegate.cpp \
    $$PWD/ViewItem/kitemdelegate.cpp \
    $$PWD/ViewItem/recent_item_delegate.cpp \
    $$PWD/ViewItem/right_item_delegate.cpp \
    $$PWD/ViewItem/tablet_full_itemdelegate.cpp \
    $$PWD/Widget/animationpage.cpp \
    $$PWD/Widget/full_commonuse_widget.cpp \
    $$PWD/Widget/full_function_widget.cpp \
    $$PWD/Widget/full_letter_widget.cpp \
    $$PWD/Widget/full_searchresult_widget.cpp \
    $$PWD/Widget/function_Widget.cpp \
    $$PWD/Widget/function_button_widget.cpp \
    $$PWD/Widget/letter_button_widget.cpp \
    $$PWD/Widget/main_view_widget.cpp \
    $$PWD/Widget/plugin_widget.cpp \
    $$PWD/Widget/splitbar_frame.cpp \
    $$PWD/Widget/tabview_widget.cpp \
    $$PWD/full_mainwindow.cpp \
    $$PWD/mainwindow.cpp \
    $$PWD/tabletwindow.cpp
