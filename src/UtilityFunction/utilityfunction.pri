INCLUDEPATH  += \
    $$PWD \
    $$PWD/Style

HEADERS += \
    $$PWD/AbstractInterface.h \
    $$PWD/KySmallPluginInterface.h \
    $$PWD/Style/style.h \
    $$PWD/abstractInterface.h \
    $$PWD/thumbnail.h \
    $$PWD/utility.h

SOURCES += \
    $$PWD/Style/style.cpp \
    $$PWD/thumbnail.cpp \
    $$PWD/utility.cpp
