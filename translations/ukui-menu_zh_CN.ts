<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>FullMainWindow</name>
    <message>
        <location filename="../src/UserInterface/full_mainwindow.cpp" line="79"/>
        <source>Search</source>
        <translation>搜索应用</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/full_mainwindow.cpp" line="138"/>
        <source>All</source>
        <translation>全部</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/full_mainwindow.cpp" line="140"/>
        <source>Letter</source>
        <translation>字母排序</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/full_mainwindow.cpp" line="142"/>
        <source>Function</source>
        <translation>功能分类</translation>
    </message>
</context>
<context>
    <name>FunctionWidget</name>
    <message>
        <location filename="../src/UserInterface/Widget/function_Widget.cpp" line="205"/>
        <source>Search</source>
        <translation>全局搜索</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/UserInterface/mainwindow.cpp" line="97"/>
        <source>Search</source>
        <translation>搜索应用</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/mainwindow.cpp" line="237"/>
        <location filename="../src/UserInterface/mainwindow.cpp" line="426"/>
        <location filename="../src/UserInterface/mainwindow.cpp" line="844"/>
        <source>All</source>
        <translation>全部</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/mainwindow.cpp" line="239"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/UserInterface/mainwindow.cpp" line="242"/>
        <source>collection</source>
        <translation>收藏</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/mainwindow.cpp" line="243"/>
        <source>recent</source>
        <translation>最近</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/mainwindow.cpp" line="244"/>
        <source>Max</source>
        <translation>放大</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/mainwindow.cpp" line="246"/>
        <source>PowerOff</source>
        <translation>关机</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/mainwindow.cpp" line="428"/>
        <location filename="../src/UserInterface/mainwindow.cpp" line="852"/>
        <source>Letter</source>
        <translation>字母排序</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/mainwindow.cpp" line="430"/>
        <location filename="../src/UserInterface/mainwindow.cpp" line="860"/>
        <source>Function</source>
        <translation>功能分类</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/BackProcess/Interface/ukuimenuinterface.cpp" line="73"/>
        <location filename="../src/UserInterface/Widget/function_button_widget.cpp" line="29"/>
        <source>Office</source>
        <translation>办公</translation>
    </message>
    <message>
        <location filename="../src/BackProcess/Interface/ukuimenuinterface.cpp" line="74"/>
        <location filename="../src/UserInterface/Widget/function_button_widget.cpp" line="30"/>
        <source>Development</source>
        <translation>开发</translation>
    </message>
    <message>
        <location filename="../src/BackProcess/Interface/ukuimenuinterface.cpp" line="75"/>
        <location filename="../src/UserInterface/Widget/function_button_widget.cpp" line="31"/>
        <source>Image</source>
        <translation>图像</translation>
    </message>
    <message>
        <location filename="../src/BackProcess/Interface/ukuimenuinterface.cpp" line="76"/>
        <location filename="../src/UserInterface/Widget/function_button_widget.cpp" line="32"/>
        <source>Video</source>
        <translation>影音</translation>
    </message>
    <message>
        <location filename="../src/BackProcess/Interface/ukuimenuinterface.cpp" line="77"/>
        <location filename="../src/UserInterface/Widget/function_button_widget.cpp" line="33"/>
        <source>Internet</source>
        <translation>网络</translation>
    </message>
    <message>
        <location filename="../src/BackProcess/Interface/ukuimenuinterface.cpp" line="78"/>
        <location filename="../src/UserInterface/Widget/function_button_widget.cpp" line="34"/>
        <source>Game</source>
        <translation>游戏</translation>
    </message>
    <message>
        <location filename="../src/BackProcess/Interface/ukuimenuinterface.cpp" line="79"/>
        <location filename="../src/UserInterface/Widget/function_button_widget.cpp" line="35"/>
        <source>Education</source>
        <translation>教育</translation>
    </message>
    <message>
        <location filename="../src/BackProcess/Interface/ukuimenuinterface.cpp" line="80"/>
        <location filename="../src/UserInterface/Widget/function_button_widget.cpp" line="36"/>
        <source>Social</source>
        <translation>社交</translation>
    </message>
    <message>
        <location filename="../src/BackProcess/Interface/ukuimenuinterface.cpp" line="81"/>
        <location filename="../src/UserInterface/Widget/function_button_widget.cpp" line="37"/>
        <source>System</source>
        <translation>系统</translation>
    </message>
    <message>
        <location filename="../src/BackProcess/Interface/ukuimenuinterface.cpp" line="82"/>
        <location filename="../src/UserInterface/Widget/function_button_widget.cpp" line="38"/>
        <source>Safe</source>
        <translation>安全</translation>
    </message>
    <message>
        <location filename="../src/BackProcess/Interface/ukuimenuinterface.cpp" line="83"/>
        <location filename="../src/UserInterface/Widget/function_button_widget.cpp" line="39"/>
        <source>Others</source>
        <translation>其他</translation>
    </message>
</context>
<context>
    <name>RightClickMenu</name>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="228"/>
        <source>Pin to all</source>
        <translation>固定到“所有软件”</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="231"/>
        <source>Unpin from all</source>
        <translation>从“所有软件”取消固定</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="241"/>
        <source>Pin to taskbar</source>
        <translation>固定到任务栏</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="244"/>
        <source>Unpin from taskbar</source>
        <translation>从任务栏取消固定</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="247"/>
        <source>Add to desktop shortcuts</source>
        <translation>添加到桌面快捷方式</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="251"/>
        <source>Pin to collection</source>
        <translation>固定到收藏</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="254"/>
        <source>Remove from collection</source>
        <translation>从收藏移除</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="269"/>
        <source>Uninstall</source>
        <translation>卸载</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="290"/>
        <source>Switch user</source>
        <translation>切换用户</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="296"/>
        <source>Hibernate</source>
        <translation>休眠</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="302"/>
        <source>Sleep</source>
        <translation>睡眠</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="306"/>
        <source>Lock Screen</source>
        <translation>锁屏</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="311"/>
        <source>Log Out</source>
        <translation>注销</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="317"/>
        <source>Restart</source>
        <translation>重启</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="323"/>
        <source>Power Off</source>
        <translation>关机</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="354"/>
        <source>Personalize this list</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TabletRightClickMenu</name>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/tabletrightclickmenu.cpp" line="241"/>
        <source>Pin to taskbar</source>
        <translation>固定到任务栏</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/tabletrightclickmenu.cpp" line="244"/>
        <source>Unpin from taskbar</source>
        <translation>从任务栏取消固定</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/tabletrightclickmenu.cpp" line="250"/>
        <source>Add to desktop shortcuts</source>
        <translation>固定到桌面快捷方式</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/tabletrightclickmenu.cpp" line="269"/>
        <source>Uninstall</source>
        <translation>卸载</translation>
    </message>
</context>
</TS>
